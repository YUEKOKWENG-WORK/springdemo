package com.optimum.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.optimum.model.Book;

@Repository("")
public class BookDAOImpl implements BookDAO {

	@Autowired
	Book bookRef;
	
	
	public Book showBook() {
		bookRef.setName("Spring Frameworks");
		return bookRef;
	}

}

package com.optimum.dao;

import com.optimum.model.Book;

public interface BookDAO {

	public Book showBook();
}

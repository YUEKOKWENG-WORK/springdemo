package com.optimum.application;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.optimum.controller.BookController;

public class BookApplication {

	public static void main(String[] args) {

		ApplicationContext contextRef = new ClassPathXmlApplicationContext("bean2.xml");
		BookController refBookController = (BookController) contextRef.getBean("myController");
		System.out.println(refBookController.getBookService().getName());
	}

}

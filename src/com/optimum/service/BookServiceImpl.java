package com.optimum.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.optimum.dao.BookDAO;
import com.optimum.model.Book;

@Service("")
public class BookServiceImpl implements BookService {

	@Autowired
	BookDAO bookDAORef;

	public void setBookDAORef(BookDAO bookDAORef) {
		this.bookDAORef = bookDAORef;
	}


	public Book callShowBook() {
		
		return bookDAORef.showBook();
	}

}

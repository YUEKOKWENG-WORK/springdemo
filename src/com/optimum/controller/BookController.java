package com.optimum.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.optimum.model.Book;
import com.optimum.service.BookService;

@Controller("myController")
public class BookController {

	@Autowired
	BookService refBookService;
	
	public Book getBookService() {
		return refBookService.callShowBook();
	}
}
